#include "netwalker.h"

NetWalker::NetWalker(QObject *parent) : QObject(parent)
{
    manager = new QNetworkAccessManager(this);
    model = new BloggerDBModel(this);
}
void NetWalker::goToAuthorProfile(QString address)
{
    QNetworkReply *reply;
    QUrl url(address);
    QNetworkRequest request(url);
    QEventLoop loopCall;
    reply = manager->get(request);
    QObject::connect(reply, SIGNAL(finished()), &loopCall, SLOT(quit()));
    loopCall.exec();
//    loopCall.deleteLater();
    if (reply->error() == QNetworkReply::NoError)
    {
        QString content= reply->readAll();
        QString uid = content.mid(content.indexOf("feeds/")+6,20);
        if(model->hasSuchAuthor(uid)) return;
        QStringList inter;
        QStringList kind;
        QStringList type;
        QStringList place;
        QStringList listURL;

        QRegExp rx = QRegExp("<span class=.favorites.>.*(?=</span>)");
        rx.setMinimal(true);
        int pos = rx.indexIn(content, 0);
        QString temp = rx.cap();
        inter = getListBySpan(temp);

        rx = QRegExp("<span class=.title.>.*(?=</span>)");
        rx.setMinimal(true);
        pos = rx.indexIn(content, 0);
        temp = rx.cap();
        kind = getListBySpan(temp);

        rx = QRegExp("<span class=.role.>.*(?=</span>)");
        rx.setMinimal(true);
        pos = rx.indexIn(content, 0);
        temp = rx.cap();
        type = getListBySpan(temp);

        rx = QRegExp("<span class=.(country-name).>.*(?=</span>)");
        rx.setMinimal(true);
        pos = rx.indexIn(content, 0);
        temp = rx.cap();
        place = getListBySpan(temp);

        rx = QRegExp("<span dir=.ltr..*(?=</span>)");
        rx.setMinimal(true);
        pos = 0;
        while ((pos = rx.indexIn(content, pos)) != -1) {
            QString tmp = rx.cap();
            listURL<<getUrlByTag(tmp);
            pos += rx.matchedLength();
        }

        model->addAuthor(uid, inter, kind, type, place);
        setBlogByAuthor(uid, listURL);

    }
    else
    {
        qDebug()<<reply->errorString();
        qDebug()<<reply->error();
    }
//    reply->deleteLater();
}
void NetWalker::setBlogByAuthor(QString uid, QStringList listURL)
{
    if(listURL.size()<1) return;
    for(int i=0;i<listURL.size();i++)
    {
        QString idBlog = getIdBlog(listURL[i]);
        model->addBlog(idBlog, listURL[i]);
        model->setRelation(uid,idBlog);
    }
}
QString NetWalker::getIdBlog(QString adress)
{
    QString id = "";
    QNetworkReply *reply;
    QUrl url(adress);
    QNetworkRequest request(url);
    QEventLoop loopCall;
    reply = manager->get(request);
    QObject::connect(reply, SIGNAL(finished()), &loopCall, SLOT(quit()));
    loopCall.exec();
    if (reply->error() == QNetworkReply::NoError)
    {
        QString location = reply->header(QNetworkRequest::LocationHeader).toString();
        QString q = location.left(location.indexOf('?'));
        QUrl locUrl(q.replace(".com",".ru"));
        QNetworkReply *rep;
        QNetworkRequest rec(locUrl);
        QEventLoop loop2;
        rep = manager->get(rec);
        QObject::connect(rep, SIGNAL(finished()), &loop2, SLOT(quit()));
        loop2.exec();
        if (reply->error() == QNetworkReply::NoError)
        {
            QString content= rep->readAll();
            QString dd = content.mid(content.indexOf("targetBlogID=")+13);
            id = dd.left(dd.indexOf('&'));
            //                return dd.split('&')[0];
        }
    }
    else
    {
        qDebug()<<reply->errorString();
        qDebug()<<reply->error();
    }
//    reply->deleteLater();
//    manager->deleteLater();
    return id;
}
QStringList NetWalker::getListBySpan(QString &span)
{
    QRegExp rx("<a .*(?=</a>)");
    rx.setMinimal(true);
    QStringList list;
    int pos = 0;
    while ((pos = rx.indexIn(span, pos)) != -1) {
        QString tmp = rx.cap();
        list<<tmp.mid(tmp.indexOf('>')+1).replace(',',"");
        pos += rx.matchedLength();
    }
    return list;
}
QString NetWalker::getUrlByTag(QString a)
{
    QString ee = a.mid(a.indexOf(QRegExp("href="))+6);
    if(ee.count(QRegExp("/profile/"))<1)
    {
        return ee.left(ee.indexOf(QRegExp("\\.com"))+4);
    }
    return ee.left(ee.indexOf(QRegExp("\">")));
}
QStringList NetWalker::getAuthorsByTag(QString tag, TypeOfTag typeOgTag, int start)
{
    QString address = QString("https://www.blogger.com/profile-find.g?t=[TAG]&q=%1&start=%2")
            .arg(tag, QString::number(start));
    QString t = "i";
    switch(typeOgTag)
    {
    case INTERESTING:
        t = "i";
        break;
    case KIND:
        t = "o";
        break;
    case TYPE:
        t = "j";
        break;
    case PLACE:
        t = "l";
        break;
    }
    QStringList result = getAuthorsFromPage(address.replace("[TAG]",t));
    return result;
}
QStringList NetWalker::getAuthorsFromPage(QString address)
{
    QNetworkReply *reply;
    QStringList listAddress;
    QUrl url(address);
    QNetworkRequest request(url);
    QEventLoop loopCall;
    reply = manager->get(request);
    QObject::connect(reply, SIGNAL(finished()), &loopCall, SLOT(quit()));
    loopCall.exec();
    if (reply->error() == QNetworkReply::NoError)
    {
        QString content= reply->readAll();


        QRegExp rx = QRegExp("<div class=.profilephoto..*(?=</div>)");
        rx.setMinimal(true);
        int pos = 0;
        while ((pos = rx.indexIn(content, pos)) != -1) {
            QString tmp = rx.cap();
            QString address = getUrlByTag(tmp);
            if(!(model->hasSuchAuthor(getUIDByAddress(address))))
                listAddress<<address;
            pos += rx.matchedLength();
        }
    }
    else
    {
        qDebug()<<reply->errorString();
        qDebug()<<reply->error();
    }
//    reply->deleteLater();
    return listAddress;
}
QString NetWalker::getUIDByAddress(QString address)
{
    return address.mid(address.indexOf("profile/")+8);
}
