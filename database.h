#ifndef DATABASE_H
#define DATABASE_H

#include <QObject>
#include <QtSql/QtSql>

#define DATABASE_HOSTNAME   "./"
#define DATABASE_NAME       "blogger.db"

class DataBase : public QObject
{
    Q_OBJECT
public:
    explicit DataBase(QObject *parent = 0);
    ~DataBase();

    void connectToDataBase();
    QSqlDatabase getDataBase();
    bool isOpen();
    void closeDataBase();
private:
    static QSqlDatabase db;
    QString connectionDBName;
private:
    bool openDataBase();
};

#endif // DATABASE_H
