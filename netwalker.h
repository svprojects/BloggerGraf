#ifndef NETWALKER_H
#define NETWALKER_H

#include <QObject>
#include <QtCore>
#include <bloggerdbmodel.h>
#include <QtNetwork>
enum TypeOfTag{
    INTERESTING = 0, //favorites
    KIND, //title - род занятий
    TYPE, //role - вид деятельности
    PLACE //страна
};

class NetWalker : public QObject
{
    Q_OBJECT
public:
    explicit NetWalker(QObject *parent = 0);
    void goToAuthorProfile(QString address);
    void setBlogByAuthor(QString uid, QStringList listURL);
    QStringList getAuthorsByTag(QString tag, TypeOfTag typeOgTag, int start=0);
private:
    QStringList getAuthorsFromPage(QString address);
    QStringList getListBySpan(QString &span);
    QString getUrlByTag(QString a);
    QString getIdBlog(QString adress);
    QString getUIDByAddress(QString address);
private:
    BloggerDBModel *model;
    QNetworkAccessManager *manager;

};

#endif // NETWALKER_H
