﻿#include <QCoreApplication>
#include <QObject>
#include <QtNetwork/QtNetwork>
#include <netwalker.h>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    NetWalker *walker = new NetWalker();
    QStringList tagList;
    tagList.clear();
    QFile file("./tags.txt");
    if(file.open(QIODevice::ReadOnly))
    {
        QTextStream out(&file);
        while(!out.atEnd())
        {
            tagList.append(out.readLine());
        }
    }
    for(int j=0;j<tagList.size();j++)
    {
        QStringList address = walker->getAuthorsByTag(tagList[j],INTERESTING);
        if(address.size()>0)
            for(int i=0;i<address.size();i++)
            {
                walker->goToAuthorProfile(address[i]);
            }
    }
    return a.exec();
}



