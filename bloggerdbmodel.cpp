#include "bloggerdbmodel.h"

BloggerDBModel::BloggerDBModel(QObject *parent) : QObject(parent)
{
    db = new DataBase(this);
    db->connectToDataBase();
}
QString BloggerDBModel::getStringFromList(QStringList list)
{
    if(list.size()<1) return "";
    return list.join(",");
}

bool BloggerDBModel::addAuthor(QString uid, QStringList intrested, QStringList kindAction, QStringList typeAction, QStringList places)
{
    if(db->isOpen())
    {
        if(!(hasSuchAuthor(uid)))
        {
            QSqlQuery query(db->getDataBase());
            QString queryString = QString("INSERT INTO authors (uid, intereses, kind_activity, type_activity, places) values ('%1', '%2', '%3', '%4', '%5');")
                    .arg(uid, getStringFromList(intrested), getStringFromList(kindAction), getStringFromList(typeAction), getStringFromList(places));
            query.exec(queryString);
            if(!(query.lastError().type() == QSqlError::NoError))
            {
                QString error = query.lastError().text();
                return false;
            }
        }
    }
    return true;
}
bool BloggerDBModel::addBlog(QString idBlog, QString url)
{
    if(db->isOpen())
    {
        if(!(hasSuchBlog(idBlog)))
        {
            QSqlQuery query(db->getDataBase());
            QString queryString = QString("INSERT INTO blogs (idBlog, url) values ('%1', '%2');")
                    .arg(idBlog, url);
            query.exec(queryString);
            if(!(query.lastError().type() == QSqlError::NoError))
                return false;
        }
    }
    return true;
}
bool BloggerDBModel::setRelation(QString idAuthor, QString idBlog)
{
    if(db->isOpen())
    {
        if(!(hasSuchRelation(idAuthor, idBlog)))
        {
            QSqlQuery query(db->getDataBase());
            QString queryString = QString("INSERT INTO author_gas_blogs (id_author, id_blogs) values ('%1', '%2');")
                    .arg(idAuthor, idBlog);
            query.exec(queryString);
            if(!(query.lastError().type() == QSqlError::NoError))
                return false;
        }
    }
    return true;
}
bool BloggerDBModel::hasSuchAuthor(QString idAuthor)
{
    if(db->isOpen())
    {
        QSqlQuery query(db->getDataBase());
        QString queryString = QString("SELECT COUNT(*) FROM authors WHERE id='%1'").arg(idAuthor);
        query.exec(queryString);
        if(!(query.lastError().type() == QSqlError::NoError))
        {
            query.first();
            int count = query.value(0).toInt();
            if (count != 0) return true;
        }
    }
    return false;
}
bool BloggerDBModel::hasSuchBlog(QString idBlog)
{
    if(db->isOpen())
    {
        QSqlQuery query(db->getDataBase());
        QString queryString = QString("SELECT COUNT(*) FROM blogs WHERE id='%1'").arg(idBlog);
        query.exec(queryString);
        if(!(query.lastError().type() == QSqlError::NoError))
        {
            query.first();
            int count = query.value(0).toInt();
            if (count != 0) return true;
        }
    }
    return false;
}
bool BloggerDBModel::hasSuchRelation(QString idAuthor, QString idBlog)
{
    if(db->isOpen())
    {
        QSqlQuery query(db->getDataBase());
        QString queryString = QString("SELECT COUNT(*) FROM author_gas_blogs WHERE id_author='%1' AND id_blogs='%2'")
                .arg(idAuthor, idBlog);
        query.exec(queryString);
        if(!(query.lastError().type() == QSqlError::NoError))
        {
            query.first();
            int count = query.value(0).toInt();
            if (count != 0) return true;
        }
    }
    return false;
}
