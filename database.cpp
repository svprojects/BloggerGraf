#include "database.h"

DataBase::DataBase(QObject *parent) : QObject(parent)
{
}
QSqlDatabase DataBase::db;

DataBase::~DataBase()
{
    closeDataBase();
}
void DataBase::connectToDataBase()
{
    this->openDataBase();
}
QSqlDatabase DataBase::getDataBase()
{
    return db;
}
bool DataBase::isOpen()
{
    return db.isOpen();
}
bool DataBase::openDataBase()
{
    if(!db.isOpen())
    {
        db =  QSqlDatabase::addDatabase("QSQLITE","bloggerDbConnection");
        db.setHostName(DATABASE_HOSTNAME);
        db.setDatabaseName(DATABASE_NAME);
        if(db.open())
            return true;
    }
    return false;
}
void DataBase::closeDataBase()
{
    db.close();
}
