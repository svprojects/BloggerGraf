#ifndef BLOGGERDBMODEL_H
#define BLOGGERDBMODEL_H

#include <QObject>
#include <QtSql>
#include <database.h>
#include <QtCore/QtCore>

class BloggerDBModel : public QObject
{
    Q_OBJECT
public:
    explicit BloggerDBModel(QObject *parent = 0);

    bool addAuthor(QString uid, QStringList intrested,
                   QStringList kindAction,
                   QStringList typeAction,
                   QStringList places);
    bool addBlog(QString idBlog, QString url);
    bool setRelation(QString idAuthor, QString idBlog);
    bool hasSuchBlog(QString idBlog);
    bool hasSuchAuthor(QString idAuthor);
    bool hasSuchRelation(QString idAuthor, QString idBlog);
private:
    QString getStringFromList(QStringList list);
private:
    DataBase *db;
};

#endif // BLOGGERDBMODEL_H
