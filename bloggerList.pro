QT += core sql network
QT -= gui

TARGET = bloggerList
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    database.cpp \
    bloggerdbmodel.cpp \
    netwalker.cpp

HEADERS += \
    database.h \
    bloggerdbmodel.h \
    netwalker.h

